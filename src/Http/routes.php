<?php

use Ieras\DcatVue\Http\Controllers;
use Illuminate\Support\Facades\Route;

// 文件存储配置表单
Route::get('dcat-filesystem-config', [Controllers\DcatVueController::class, 'filesystem']);
// 各个驱动直传配置
Route::get('ieras/file/obs-config', [Controllers\DcatVueController::class, 'obsConfig'])
    ->name('ieras.file.obs-config');
// 文件上传后回调
Route::post('ieras/file/uploaded', [Controllers\DcatVueController::class, 'uploaded'])
    ->name('ieras.file.uploaded');
// 本地上传
Route::post('ieras/file/upload', [Controllers\DcatVueController::class, 'upload'])
    ->name('ieras.file.upload');
// 区划
Route::get('ieras/distpicker/regions', [Controllers\DcatVueController::class, 'regions'])
    ->name('ieras.distpicker.regions');
// 地址解析
Route::get('ieras/distpicker/address2ll', [Controllers\DcatVueController::class, 'address2ll'])
    ->name('ieras.distpicker.address2ll');
// 坐标位置信息
Route::get('ieras/distpicker/ll2address', [Controllers\DcatVueController::class, 'll2address'])
    ->name('ieras.distpicker.ll2address');
