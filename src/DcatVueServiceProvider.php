<?php

namespace Ieras\DcatVue;

use Dcat\Admin\Extend\ServiceProvider;
use Dcat\Admin\Admin;
use Dcat\Admin\Form;
use Illuminate\Support\Facades\Event;
use Ieras\DcatVue\Field\DateRange;
use Ieras\DcatVue\Field\Distpicker;
use Ieras\DcatVue\Field\File;
use Ieras\DcatVue\Field\Image;
use Ieras\DcatVue\Field\KeyValue;
use Ieras\DcatVue\Field\ListField;
use Ieras\DcatVue\Field\MultipleFile;
use Ieras\DcatVue\Field\MultipleImage;
use Ieras\DcatVue\Field\MultipleSelect;
use Ieras\DcatVue\Field\Select;
use Ieras\DcatVue\Field\Tag;

class DcatVueServiceProvider extends ServiceProvider
{
	protected $js = [
        'js/helper.js',
    ];
	protected $css = [
		'css/index.css',
	];

    protected $exceptRoutes = [
        'permission' => ['ieras*'],//不需要权限的路由
        //'auth' => ['ieras*']//不需要登录的路由
    ];

	public function register()
	{
		//
	}

	public function init()
	{
		parent::init();

        $this->loadMigrationsFrom(__DIR__ . '/../migrations');

        $this->hackConfigs();

//		Form::extend('vue', Vue::class);
		Form::extend('vFile', File::class);
		Form::extend('vMultipleFile', MultipleFile::class);
        Form::extend('vImage', Image::class);
        Form::extend('vMultipleImage', MultipleImage::class);
        Form::extend('vTags', Tag::class);
        Form::extend('vList', ListField::class);
        Form::extend('vKeyValue', KeyValue::class);
        Form::extend('vDistpicker', Distpicker::class);
        Form::extend('vDateRange', DateRange::class);
        Form::extend('vSelect', Select::class);
        Form::extend('vMultipleSelect', MultipleSelect::class);

        Admin::requireAssets('@ieras.dcat-vue');
	}

	public function settingForm()
	{
		return new Setting($this);
	}

    protected function hackConfigs()
    {
        if (is_file(app()->getCachedConfigPath())) {
            return;
        }

        $configs = collect(admin_setting_array('ieras_filesystem'));

//        dd($configs);

        app()->booted(function () use ($configs) {
            Helper::injectFilesystemConfig($configs);
        });

        Event::listen('admin:booted', function () use ($configs) {
            config()->set('admin.upload.disk', $configs->get('disk', config('admin.upload.disk')));
        });
    }
}
