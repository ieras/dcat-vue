# Dcat Admin Extension

##### 此扩展为大合一扩展，以后使用vue3构建的组件都将合并在一起

### 演示地址(weiwait大佬原版的演示地址)
[demo: http://dcat.weiwait.cn (admin:admin)](http://dcat.weiwait.cn/admin/demo-settings 'user: admin psw: admin')

### 依赖扩展
[overtrue/laravel-filesystem-cos](https://github.com/overtrue/laravel-filesystem-cos)

[zgldh/qiniu-laravel-storage](https://github.com/zgldh/qiniu-laravel-storage)

[iiDestiny/laravel-filesystem-oss](https://github.com/iiDestiny/laravel-filesystem-oss)

### 通过 composer 安装扩展
```shell
  composer require ieras/dcat-vue
```

### 文件系统-通过选项卡使用
```php
    public function index(Content $content): Content
    {
        $tab = Tab::make();
        $tab->add('文件存储', new \Ieras\DcatVue\Forms\FilesystemConfig());

        return $content->title('配置')
            ->body($tab->withCard());
    }
```

[//]: # (### 文件系统-通过一级菜单使用)

[//]: # ()
[//]: # (![]&#40;https://github.com/weiwait/images/blob/main/dcat-smtp-menu.png?raw=true&#41;)

##### 文件系统
![文件系统](https://raw.githubusercontent.com/weiwait/images/main/dcat-filesystem-config.png)
##### 行政区划、坐标
![区划选择](https://raw.githubusercontent.com/weiwait/images/main/dcat-v-distpicker.png)
##### 图片裁剪(支持多图)
![区划选择](https://raw.githubusercontent.com/weiwait/images/main/dcat-v-cropper.png)
##### 日期范围
![区划选择](https://raw.githubusercontent.com/weiwait/images/main/dcat-v-daterange.png)
##### 列表、选择
![区划选择](https://raw.githubusercontent.com/weiwait/images/main/dcat-v-other.png)

### 已有表单组件(采用的是Naive UI)
```php
    $form->vFile('file') // 关联文件系统配置-直传
        ->accept('mime types');
        
    $form->vMutipleFile('files') // 关联文件系统配置-直传
        ->accept('mime types');
        
    $form->vImage('image') // 关联文件系统配置-直传-裁剪
        ->ratio(16 / 9) // 固定裁剪比例
        ->large() // 放大裁剪框
        ->resolution(1920, 1080) // 重置图片分辨率
        ->jpeg(0.8) // 裁剪为jpeg格式, 参数为图片质量0-1
        ->accept('mime types');
        
    $form->vMultipleImage('images') // 关联文件系统配置-直传-裁剪
        ->ratio(16 / 9) // 固定裁剪比例
        ->large() // 放大裁剪框
        ->resolution(1920, 1080) // 重置图片分辨率
        ->jpeg(0.8) // 裁剪为jpeg格式, 参数为图片质量0-1
        ->accept('mime types');
        
    $form->vTags('tags'); // 标签
    
    $form->vList('list')
        ->sortable() // 开启排序
        ->max(8); // 限制最大添加数量
        
    $form->vKeyValue('kvs')
        ->sortable() // 开启排序
        ->serial() // 开启固定有序索引 默认为字母A-Z
        ->keys(['一', '二', '三', '四']) // serial后自定义索引
        ->list(); // serial后只提交值，保存为一维数组(索引仅作为显示)
        
    $form->vDistpicker('region')
        ->dist('province', 'city', 'district') // 开启区划
        ->coordinate('latitude', 'longitude') // 开启坐标
        ->detail('detail') // 开启详细地址
        ->disableMap() // 关闭地图
        ->mapHeight(380) // 地图高度，默认380
        ->disableRegions([440000]) // 禁用一些区划
        ->mapZoom(11); // 地图默认缩放
        ->mapZoom(11, 'zoom') // 记录地图缩放级别

    $form->vSelect('select')
        ->options(['123', '456', 'A' => 'aaa']) // 选项
        ->concatKey('separator') // 显示键
        ->optionsFromKeyValue('kvs'); // 用于结合vKeyValue进行选项选择

    $form->vMultipleSelect('ms', '多选')
        ->options(['123', '456', 'A' => 'aaa']) // 选项
        ->concatKey('separator') // 显示键
        ->optionsFromKeyValue('kvs'); // 用于结合vKeyValue进行选项选择
```
### 若希望ide可以跳转方法(不展示黄色)，可以配置一下帮助文件(dcat_admin_ide_helper.php)
```php
namespace Dcat\Admin {
   /**
     * @method \Ieras\DcatVue\Field\File vFile($column, $label = '')
     * @method \Ieras\DcatVue\Field\MultipleFile vMultipleFile($column, $label = '')
     * @method \Ieras\DcatVue\Field\Image vImage($column, $label = '')
     * @method \Ieras\DcatVue\Field\MultipleImage vMultipleImage($column, $label = '')
     * @method \Ieras\DcatVue\Field\Tag vTags($column, $label = '')
     * @method \Ieras\DcatVue\Field\ListField vList($column, $label = '')
     * @method \Ieras\DcatVue\Field\KeyValue vKeyValue($column, $label = '')
     * @method \Ieras\DcatVue\Field\Distpicker vDistpicker($column, $label = '')
     * @method \Ieras\DcatVue\Field\DateRange vDateRange($column, $label = '')
     * @method \Ieras\DcatVue\Field\Select vSelect($column, $label = '')
     * @method \Ieras\DcatVue\Field\MultipleSelect vMultipleSelect($column, $label = '')
     *
     */
    class Form {}

}
```

### 若有好建议，可以直接在 [issues](https://gitee.com/ieras/dcat-vue/issues) 提问
当然，也可以加我微信`i_eras`加上我后，给它发送"互联网路漫长"，它便会拉你进php技术交流群，艾特群主就找到我了。不过群主也是打工族，往往比较忙，
希望不要过于打扰，感谢！最后再次感谢大佬weiwait提供的扩展，本人只是因为不太满足需求，再他基础上进行了调整，不过，后期我会跟着大神原版持续更新的。

[comment]: <> (### Donate)

[comment]: <> (![示例图片]&#40;https://github.com/weiwait/images/blob/main/donate.png?raw=true&#41;)

### Dcat-admin 扩展列表(weiwait 开发)

[//]: # (1. [图片裁剪]&#40;https://github.com/weiwait/dcat-cropper&#41;)

[//]: # (2. [区划级联+坐标拾取]&#40;https://github.com/weiwait/dcat-distpicker&#41;)
3. [smtp 便捷配置](https://github.com/weiwait/dcat-smtp)
4. [sms channel 便捷配置](https://github.com/weiwait/dcat-easy-sms)
